const webpush = require('web-push')
const express = require('express')
const cors = require('cors')
const bodyParser = require('body-parser')
const app = express()

const port = 4000

// ExpressJS
app.use(cors())
app.use(bodyParser.json())
app.listen(port, () => console.log(`Escuchando en el puerto ${port}...`))

// En este ejemplo simplemente guardamos la suscripción que llegue en una variable,
// en un caso real guardaríamos las suscripciones en base de datos junto con los datos asociados que necesitemos
const fakeDB = { suscripcion: null }
const guardarSuscripcion = async sus => {
  fakeDB.suscripcion = sus
}

// La librería web-push nos genera las claves pública y privada y será la encargada de enviar la notificación
// Para generar las claves: web-push generate-vapid-keys
const vapidKeys = {
  publicKey: 'BHUEh23RK0OAIGfRA4G_B0d2ceyH0eSG2qK1tTALcuw_wVYom-2IwbjJUUKNK6F7QJhiZ3fPSySSYp8fZin-Ghg',
  privateKey: 'uzcXoyqeLzC3rZ3DDGsSklmgjUPRXuK0nkWD7jXO42E'
}
webpush.setVapidDetails(
  'mailto:admin@midominio.com',
  vapidKeys.publicKey,
  vapidKeys.privateKey
)

// La librería web-push nos sirve para enviar una notificación
// (previamente tenemos que haber establecido las claves VAPID)
function enviaNotificacion(sus, datos = '') {
  webpush.sendNotification(sus, datos)
}

// Endpoint para recibir una suscripción enviada desde nuestra app
app.post('/guardar-suscripcion', async (req, res) => {
  const suscripcion = req.body
  console.log(suscripcion)
  await guardarSuscripcion(suscripcion)
  res.json({ msj: 'ok' })
})

// Endpoint para enviar la notificación PUSH a los Service Workers de los clientes suscritos
app.get('/enviar-notificacion', (req, res) => {
  const sus = fakeDB.suscripcion
  const mensaje = 'Mensaje de prueba'
  enviaNotificacion(sus, mensaje)
  res.json({ msj: 'Mensaje enviado:', mensaje })
})

